#!/bin/bash

docker-compose exec -e APP_ENV=testing app php vendor/bin/codecept "$@"
