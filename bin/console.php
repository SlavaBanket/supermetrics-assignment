<?php

use App\Console\Kernel\Kernel;
use Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

$dotEnv = Dotenv::createUnsafeImmutable(__DIR__ . '/../');
$dotEnv->load();

$kernel = new Kernel();
$kernel->bootstrap();
try {
    $status = $kernel->execute($argv);
} catch (Throwable $ex) {
    echo $ex->getMessage() . PHP_EOL;
    $status = 1;
}

exit($status);
