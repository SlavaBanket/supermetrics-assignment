<?php

use App\Cache\Client\Factory\RedisClientFactory;
use App\Cache\Driver\CacheDriverInterface;
use App\Cache\Driver\Factory\CacheDriverFactory;
use Statistics\Aggregator\Factory\StatisticsAggregatorFactory;
use Statistics\Aggregator\StatisticsAggregator;
use Statistics\Console\Output\Factory\StatisticsRenderFactory;
use Statistics\Console\Output\StatisticsRender;
use Statistics\Service\Factory\StatisticsServiceFactory;
use Statistics\Service\StatisticsService;
use Supermetrics\Driver\Factory\SupermetricsDriverFactory;
use Supermetrics\Driver\SupermetricsDriver;

return [
    'factories' => [
        'redis.client'              => RedisClientFactory::class,
        CacheDriverInterface::class => CacheDriverFactory::class,
        SupermetricsDriver::class   => SupermetricsDriverFactory::class,
        StatisticsService::class    => StatisticsServiceFactory::class,
        StatisticsAggregator::class => StatisticsAggregatorFactory::class,
        StatisticsRender::class     => StatisticsRenderFactory::class,
    ],
];
