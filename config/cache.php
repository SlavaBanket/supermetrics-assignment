<?php

return [
    'driver'       => env('CACHE_DRIVER', 'redis'),
    'redis'        => [
        'host'     => env('REDIS_HOST'),
        'port'     => env('REDIS_PORT'),
        'password' => env('REDIS_PASSWORD'),
    ],
    'supermetrics' => [
        'posts' => [
            'lifetime' => env('SUPERMETRICS_POST_CACHE_LIFETIME'),
        ],
        'token' => [
            'lifetime' => env('SUPERMETRICS_TOKEN_CACHE_LIFETIME'),
        ],
    ],
];
