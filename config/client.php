<?php

return [
    'supermetrics' => [
        'host' => env('SUPERMETRICS_HOST'),
        'auth' => [
            'client_id' => env('SUPERMETRICS_CLIENT_ID'),
            'email'     => env('SUPERMETRICS_EMAIL'),
            'name'      => env('SUPERMETRICS_NAME'),
        ],
    ],
];
