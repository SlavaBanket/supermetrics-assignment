<?php

use Statistics\Console\Command\StatisticsCommand;

return [
    'commands' => [
        StatisticsCommand::class,
    ],
];