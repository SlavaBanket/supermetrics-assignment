<?php

if (!function_exists('env')) {
    /**
     * Method getenv() returns string or false.
     * Use this method instead of getenv() if you want to get typed value from .env or default value.
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed|null
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return $default;
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return null;
        }

        if (($valueLength = strlen($value)) > 1 && $value[0] === '"' && $value[$valueLength - 1] === '"') {
            return substr($value, 1, -1);
        }

        return $value;
    }
}

if (!function_exists('isInteger')) {
    /**
     * is_int('2') returns false. isInteger('2') returns true
     * @param $input
     *
     * @return bool
     */
    function isInteger($input)
    {
        return (ctype_digit(strval($input)));
    }
}
