<?php

namespace App\Cache\Client\Factory;

use App\Config\Config;
use App\Factory\FactoryInterface;
use InvalidArgumentException;
use Redis;

class RedisClientFactory implements FactoryInterface
{
    /**
     * @throws InvalidArgumentException
     *
     * @return Redis
     */
    public function factory(): Redis
    {
        $config = Config::get('cache.redis');

        $client = new Redis();
        $client->connect($config['host'], $config['port']);
        if (null !== $config['password']) {
            $client->auth($config['password']);
        }

        return $client;
    }
}
