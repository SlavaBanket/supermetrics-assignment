<?php

namespace App\Cache\Decorator;

use App\Cache\Driver\CacheDriverInterface;

class CacheDecorator
{
    /**
     * @var CacheDriverInterface
     */
    private CacheDriverInterface $driver;

    /**
     * @param CacheDriverInterface $driver
     */
    public function __construct(CacheDriverInterface $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @param callable $handler
     * @param string   $key
     * @param int|null $lifetime
     *
     * @return mixed
     */
    public function getData(callable $handler, string $key, ?int $lifetime = null)
    {
        $data = $this->driver->get($key);
        if (false === $data) {
            $data = $handler();
            $this->driver->set($key, $data, $lifetime);
        }

        return $data;
    }

    /**
     * @param array $prefix
     * @param array $params
     *
     * @return string
     */
    public function assembleCacheKey(array $prefix = [], array $params = []): string
    {
        return implode(':', $prefix) . ':' . $this->calcHash($params);
    }

    /**
     * @param array $params
     *
     * @return string
     */
    private function calcHash(array $params = []): string
    {
        $data = serialize($params);

        return hash('sha256', $data);
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function delete(string $key): bool
    {
        return $this->driver->delete($key);
    }
}
