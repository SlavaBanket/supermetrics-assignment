<?php

namespace App\Cache\Driver;

interface CacheDriverInterface
{
    /**
     * @param string $key
     *
     * @return mixed|null
     */
    public function get(string $key);

    /**
     * @param string   $key
     * @param mixed    $value
     * @param int|null $lifetime
     *
     * @return bool
     */
    public function set(string $key, $value, int $lifetime = null): bool;

    /**
     * @param string $key
     *
     * @return bool
     */
    public function delete(string $key): bool;
}