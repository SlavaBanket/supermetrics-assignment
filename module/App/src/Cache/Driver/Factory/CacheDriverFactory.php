<?php

namespace App\Cache\Driver\Factory;

use App\Cache\Driver\CacheDriverInterface;
use App\Cache\Driver\RedisDriver;
use App\Config\Config;
use App\Container\Container;
use App\Factory\FactoryInterface;
use InvalidArgumentException;

class CacheDriverFactory implements FactoryInterface
{
    private const REDIS = 'redis';

    /**
     * @throws InvalidArgumentException
     *
     * @return CacheDriverInterface
     */
    public function factory(): CacheDriverInterface
    {
        $type = Config::get('cache.driver');

        switch ($type) {
            case self::REDIS:
                $client = Container::get('redis.client');

                return new RedisDriver($client);
            default:
                throw new InvalidArgumentException('Invalid cache type');
        }
    }
}
