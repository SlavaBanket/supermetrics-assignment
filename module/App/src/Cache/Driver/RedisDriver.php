<?php

namespace App\Cache\Driver;

use Redis;
use RuntimeException;

class RedisDriver implements CacheDriverInterface
{
    /**
     * @var Redis
     */
    private Redis $client;

    /**
     * @param Redis $client
     */
    public function __construct(Redis $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key)
    {
        $data      = $this->client->get($key);
        $lastError = $this->client->getLastError();

        if (null !== $lastError) {
            throw new RuntimeException($lastError);
        }

        if (false === $data) {
            return false;
        }

        return unserialize($data);
    }

    /**
     * @param string   $key
     * @param mixed    $value
     * @param int|null $lifetime
     *
     * @return bool
     */
    public function set(string $key, $value, int $lifetime = null): bool
    {
        return $this->client->set($key, serialize($value), $lifetime);
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function delete(string $key): bool
    {
        return (bool)$this->client->del($key);
    }
}
