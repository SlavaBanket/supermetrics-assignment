<?php

namespace App\Client\Dto;

class Request
{
    /**
     * @var string
     */
    private string $method;

    /**
     * @var string
     */
    private string $url;

    /**
     * @var array
     */
    private array $query = [];

    /**
     * @var array
     */
    private array $body = [];

    /**
     * @param string $method
     * @param string $url
     */
    public function __construct(string $method, string $url)
    {
        $this->method = $method;
        $this->url    = $url;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return array
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @param array $query
     *
     * @return $this
     */
    public function setQuery(array $query): self
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

    /**
     * @param array $body
     *
     * @return $this
     */
    public function setBody(array $body): self
    {
        $this->body = $body;

        return $this;
    }

}
