<?php

namespace App\Client\Enum;

class HttpMethodEnum
{
    public const GET  = 'GET';

    public const POST = 'POST';
}
