<?php

namespace App\Client\Exception;

use Exception;

class HttpClientException extends Exception
{
    /**
     * @var string|null
     */
    protected ?string $response;

    /**
     * @param string      $message
     * @param string|null $response
     *
     * @return static
     */
    public static function create(string $message, string $response = null)
    {
        $ex           = new static($message);
        $ex->response = $response;

        return $ex;
    }

    /**
     * @return string|null
     */
    public function getResponse(): ?string
    {
        return $this->response;
    }
}
