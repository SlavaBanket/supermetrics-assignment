<?php

namespace App\Client;

use App\Client\Dto\Request;
use App\Client\Enum\HttpMethodEnum;
use App\Client\Exception\HttpClientException;

class HttpClient implements HttpClientInterface
{
    /**
     * @var string
     */
    private string $host;

    /**
     * @param Request $request
     *
     * @throws HttpClientException
     *
     * @return array
     */
    public function send(Request $request): array
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);

        switch ($request->getMethod()) {
            case HttpMethodEnum::GET:
                $result = $this->get($ch, $request);
                break;
            case HttpMethodEnum::POST:
                $result = $this->post($ch, $request);
                break;
            default:
                throw new HttpClientException(sprintf('Method %s not supported', $request->getMethod()));
        }

        return $this->handleResponse($result, $ch);
    }

    /**
     * @param string $host
     *
     * @return $this
     */
    public function setHost(string $host): self
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @param         $ch
     * @param Request $request
     *
     * @return bool|string
     */
    private function get($ch, Request $request)
    {
        $url = $this->getUrl($request->getUrl());
        if (!empty($request->getQuery())) {
            $url .= '?' . http_build_query($request->getQuery());
        }
        curl_setopt($ch, CURLOPT_URL, $url);

        return curl_exec($ch);
    }

    /**
     * @param         $ch
     * @param Request $request
     *
     * @return bool|string
     */
    private function post($ch, Request $request)
    {
        $body = json_encode($request->getBody());

        curl_setopt($ch, CURLOPT_URL, $this->getUrl($request->getUrl()));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($body),
                'Accept: application/json',
            ]
        );

        return curl_exec($ch);
    }

    /**
     * @param bool|string $result
     * @param             $ch
     *
     * @throws HttpClientException
     *
     * @return array
     */
    private function handleResponse($result, $ch): array
    {
        if (false === $result) {
            $error = curl_error($ch);
            curl_close($ch);

            throw new HttpClientException($error);
        }

        $responseCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        if (400 <= $responseCode) {
            curl_close($ch);

            throw HttpClientException::create(sprintf('Code %s was sent', $responseCode), $result);
        }

        curl_close($ch);

        $result = json_decode($result, true);
        if (false === $result) {
            throw new HttpClientException(json_last_error_msg());
        }

        return $result;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    private function getUrl(string $url): string
    {
        if (empty($this->host)) {
            return $url;
        }

        return $this->host . $url;
    }
}
