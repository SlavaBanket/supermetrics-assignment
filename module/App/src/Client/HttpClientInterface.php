<?php

namespace App\Client;

use App\Client\Dto\Request;

interface HttpClientInterface
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function send(Request $request): array;
}