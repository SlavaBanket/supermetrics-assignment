<?php

namespace App\Config;

class Config
{
    /**
     * @var array
     */
    private static array $config = [];

    /**
     * @return void
     */
    public static function load(): void
    {
        $files = glob('config/*.php');
        if (false === $files) {
            return;
        }

        foreach ($files as $file) {
            static::$config[basename($file, '.php')] = require $file;
        }
    }

    /**
     * @param string     $key
     * @param mixed|null $default
     *
     * @return mixed
     */
    public static function get(string $key, $default = null)
    {
        if (strpos($key, '.') === false) {
            return array_key_exists($key, static::$config) ? static::$config[$key] : $default;
        }

        $keys   = explode('.', $key);
        $result = static::$config;
        foreach ($keys as $key) {
            if (is_array($result) && array_key_exists($key, $result)) {
                $result = $result[$key];
            } else {
                return $default;
            }
        }

        return $result;
    }
}
