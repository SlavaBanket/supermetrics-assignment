<?php

namespace App\Console\Command;

use InvalidArgumentException;

abstract class Command implements CommandInterface
{
    /**
     * @var string
     */
    protected string $signature;

    /**
     * @var string
     */
    protected string $description;

    /**
     * @return string
     */
    public function getSignature(): string
    {
        return $this->signature;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        if (empty($this->signature)) {
            throw new InvalidArgumentException('Signature is required');
        }

        return explode(' ', $this->signature)[0];
    }
}
