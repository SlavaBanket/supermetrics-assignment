<?php

namespace App\Console\Command;

use App\Console\Input\ConsoleInput;

interface CommandInterface
{
    /**
     * @param ConsoleInput $input
     *
     * @return int
     */
    public function handle(ConsoleInput $input): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getSignature(): string;

    /**
     * @return string
     */
    public function getDescription(): string;
}
