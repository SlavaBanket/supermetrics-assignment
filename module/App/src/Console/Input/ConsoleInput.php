<?php

namespace App\Console\Input;

use InvalidArgumentException;

class ConsoleInput
{
    /**
     * @var array
     */
    private array $input;

    /**
     * @param array $input
     */
    public function __construct(array $input)
    {
        $this->input = $this->prepare($input);
    }

    /**
     * @param string     $key
     * @param mixed|null $default
     *
     * @return mixed|null
     */
    public function get(string $key, $default = null)
    {
        return array_key_exists($key, $this->input) ? $this->input[$key] : $default;
    }

    /**
     * @param array $input
     *
     * @return array
     */
    private function prepare(array $input): array
    {
        $params = [];
        foreach ($input as $item) {
            $parts = explode('=', $item, 2);
            [$name, $value] = [$parts[0], $parts[1] ?? null];

            if (empty($value)) {
                throw new InvalidArgumentException(sprintf('Value of parameter `%s` is required', $name));
            }
            $params[$name] = $value;
        }

        return $params;
    }
}
