<?php

namespace App\Console\Kernel;

use App\Config\Config;
use App\Console\Command\CommandInterface;
use App\Console\Input\ConsoleInput;
use App\Console\Output\View\CommandListView;
use App\Container\Container;
use InvalidArgumentException;

class Kernel
{
    /**
     * @var CommandInterface[]
     */
    private array $commands;

    /**
     * @return void
     */
    public function bootstrap(): void
    {
        $this->loadHelpers();
        Config::load();
        $this->loadCommands();
    }

    /**
     * @param array $argv
     *
     * @return int
     */
    public function execute(array $argv): int
    {
        array_shift($argv);
        if (empty($argv[0])) {
            $this->displayList();

            return 0;
        }

        $name = $argv[0];
        array_shift($argv);
        if (!array_key_exists($name, $this->commands)) {
            throw new InvalidArgumentException('Command not found');
        }

        return $this->commands[$name]->handle(new ConsoleInput($argv));
    }

    /**
     * @return void
     */
    private function loadCommands(): void
    {
        $commands = Config::get('console.commands');
        foreach ($commands as $commandClass) {
            $command = Container::get($commandClass);
            if (!$command instanceof CommandInterface) {
                throw new InvalidArgumentException(
                    sprintf('Command %s should implement %s', get_class($command), CommandInterface::class)
                );
            }

            if (isset($this->commands[$command->getName()])) {
                throw new InvalidArgumentException(
                    sprintf(
                        'Command %s has the same name with command %s',
                        get_class($command),
                        get_class($this->commands[$command->getName()])
                    )
                );
            }

            $this->commands[$command->getName()] = $command;
        }
    }

    /**
     * @return void
     */
    private function displayList(): void
    {
        (new CommandListView())->render($this->commands);
    }

    /**
     * @return void
     */
    private function loadHelpers(): void
    {
        include 'helpers/helper.php';
    }
}
