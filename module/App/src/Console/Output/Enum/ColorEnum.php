<?php

namespace App\Console\Output\Enum;

class ColorEnum
{
    public const RED   = 31;
    public const GREEN = 32;
    public const BLUE  = 34;
}