<?php

namespace App\Console\Output\Helper;

class ColorWrapper
{
    private const START = "\e[";

    private const END = "\e[0m";

    /**
     * @param string $text
     * @param int    $color
     *
     * @return string
     */
    public static function wrap(string $text, int $color): string
    {
        return sprintf('%s%dm%s%s', self::START, $color, $text, self::END);
    }
}
