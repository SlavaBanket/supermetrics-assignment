<?php

namespace App\Console\Output\View;

use App\Console\Command\CommandInterface;
use App\Console\Output\Enum\ColorEnum;
use App\Console\Output\Helper\ColorWrapper;

class CommandListView
{
    /**
     * @param CommandInterface[] $commands
     *
     * @return void
     */
    public function render(array $commands): void
    {
        $max = $this->getMaxSignature($commands);
        if (0 === $max) {
            $max = strlen('Command');
        }

        $header = sprintf('%s %s', str_pad('COMMAND', $max), 'DESCRIPTION');
        echo PHP_EOL . ColorWrapper::wrap($header, ColorEnum::RED) . PHP_EOL;
        foreach ($commands as $command) {
            echo ColorWrapper::wrap(str_pad($command->getSignature(), $max), ColorEnum::GREEN);
            echo ' ' . str_replace(PHP_EOL, PHP_EOL . str_pad('', $max + 1), $command->getDescription()) . PHP_EOL;
        }
        echo PHP_EOL;
    }

    /**
     * @param CommandInterface[] $commands
     *
     * @return int
     */
    private function getMaxSignature(array $commands): int
    {
        $max = 0;
        foreach ($commands as $command) {
            $length = strlen($command->getSignature());
            if ($length > $max) {
                $max = $length;
            }
        }

        return $max;
    }
}
