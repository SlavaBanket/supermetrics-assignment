<?php

namespace App\Container;

use App\Config\Config;
use App\Factory\FactoryInterface;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;

class Container
{
    /**
     * @var array
     */
    private static array $instances = [];

    /**
     * @param string $class
     *
     * @return mixed
     */
    public static function get(string $class)
    {
        if (empty(self::$instances[$class])) {
            $config = Config::get('app');
            if (isset($config['factories'][$class])) {
                static::fromFactory($config['factories'][$class], $class);
            } else {
                static::fromReflection($class);
            }
        }

        return self::$instances[$class];
    }

    /**
     * @param string $factoryClass
     * @param string $class
     *
     * @return void
     */
    private static function fromFactory(string $factoryClass, string $class): void
    {
        /** @var FactoryInterface $factory */
        $factory                 = new $factoryClass();
        self::$instances[$class] = $factory->factory();
    }

    /**
     * @param string $class
     *
     * @throws InvalidArgumentException
     *
     * @return void
     */
    private static function fromReflection(string $class): void
    {
        try {
            $reflectionClass = new ReflectionClass($class);
        } catch (ReflectionException $ex) {
            throw new InvalidArgumentException(sprintf('Unable to create instance of %s', $class));
        }

        $constructor = $reflectionClass->getConstructor();
        if (null === $constructor) {
            self::$instances[$class] = new $class();

            return;
        }

        $dependencies        = $constructor->getParameters();
        $dependencyInstances = [];
        foreach ($dependencies as $dependency) {
            if (null === $dependency->getClass()) {
                throw new InvalidArgumentException(
                    sprintf(
                        'Argument %s in %s is not class. Please create factory',
                        $dependency->getName(),
                        $class
                    )
                );
            }
            $dependencyInstances[] = static::get($dependency->getClass()->getName());
        }

        $instance                = $reflectionClass->newInstanceArgs($dependencyInstances);
        self::$instances[$class] = $instance;
    }
}
