<?php

namespace App\Factory;

interface FactoryInterface
{
    /**
     * @return mixed
     */
    public function factory();
}
