<?php

namespace Statistics\Adapter;

use Generator;

interface PostAdapterInterface
{
    /**
     * @param int $page
     *
     * @return Generator|null
     */
    public function getPosts(int $page): ?Generator;
}
