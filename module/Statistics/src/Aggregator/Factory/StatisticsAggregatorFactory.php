<?php

namespace Statistics\Aggregator\Factory;

use App\Container\Container;
use App\Factory\FactoryInterface;
use Statistics\Aggregator\Handler\AverageLengthHandler;
use Statistics\Aggregator\Handler\AverageNumberPostsPerUserHandler;
use Statistics\Aggregator\Handler\LongestPostHandler;
use Statistics\Aggregator\Handler\TotalPostsByWeekHandler;
use Statistics\Aggregator\StatisticsAggregator;

class StatisticsAggregatorFactory implements FactoryInterface
{
    private const MAP = [
        AverageLengthHandler::class,
        AverageNumberPostsPerUserHandler::class,
        LongestPostHandler::class,
        TotalPostsByWeekHandler::class,
    ];

    /**
     * @return StatisticsAggregator
     */
    public function factory(): StatisticsAggregator
    {
        $aggregator = new StatisticsAggregator();
        foreach (self::MAP as $handler) {
            $aggregator->addHandler(Container::get($handler));
        }

        return $aggregator;
    }
}
