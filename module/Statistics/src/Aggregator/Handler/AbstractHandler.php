<?php

namespace Statistics\Aggregator\Handler;

use Statistics\Dto\PostDto;
use Statistics\Dto\StatisticsDto;

abstract class AbstractHandler implements HandlerInterface
{
    /**
     * @param PostDto $item
     *
     * @return void
     */
    public function handle(PostDto $item): void
    {
        if (!$this->check($item)) {
            return;
        }

        $this->doHandle($item);
    }

    /**
     * @return StatisticsDto
     */
    public function aggregate(): StatisticsDto
    {
        $dto = new StatisticsDto();
        $dto->setType($this->getType())
            ->setStatistics($this->doAggregate());

        return $dto;
    }

    /**
     * @param PostDto $item
     *
     * @return bool
     */
    protected function check(PostDto $item): bool
    {
        return null !== $item->getCreatedTime();
    }

    /**
     * @param PostDto $item
     *
     * @return void
     */
    abstract protected function doHandle(PostDto $item): void;

    /**
     * @return mixed
     */
    abstract protected function doAggregate();
}
