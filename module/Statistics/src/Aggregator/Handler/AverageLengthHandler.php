<?php

namespace Statistics\Aggregator\Handler;

use Statistics\Dto\PostDto;
use Statistics\Enum\StatisticsTypeEnum;

class AverageLengthHandler extends AbstractHandler
{
    /**
     * @var array
     */
    private array $statistics = [];

    /**
     * @param PostDto $item
     *
     * @return void
     */
    protected function doHandle(PostDto $item): void
    {
        $month = $item->getCreatedTime()->format('m');
        if (!isset($this->statistics[$month])) {
            $this->statistics[$month] = ['count' => 0, 'length' => 0];
        }

        $this->statistics[$month]['count']++;
        $this->statistics[$month]['length'] += strlen($item->getMessage());
    }

    /**
     * @return mixed
     */
    protected function doAggregate()
    {
        $result = [];
        foreach ($this->statistics as $month => $monthStatistics) {
            $result[$month] = $monthStatistics['count'] > 0 ? $monthStatistics['length'] / $monthStatistics['count'] : 0;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return StatisticsTypeEnum::AVERAGE_LENGTH;
    }
}
