<?php

namespace Statistics\Aggregator\Handler;

use Statistics\Dto\PostDto;
use Statistics\Enum\StatisticsTypeEnum;

class AverageNumberPostsPerUserHandler extends AbstractHandler
{
    /**
     * @var array
     */
    private array $statistics = [];

    /**
     * @param PostDto $item
     *
     * @return void
     */
    public function doHandle(PostDto $item): void
    {
        $month = $item->getCreatedTime()->format('m');
        $user  = $item->getFromId();
        if (!isset($this->statistics[$month][$user])) {
            $this->statistics[$month][$user] = 0;
        }

        $this->statistics[$month][$user]++;
    }

    /**
     * @return mixed
     */
    protected function doAggregate()
    {
        $result = [];
        foreach ($this->statistics as $month => $monthStatistics) {
            $count          = count($monthStatistics);
            $result[$month] = $count > 0 ? array_sum($monthStatistics) / $count : 0;
        }

        return $result;
    }

    /**
     * @param PostDto $item
     *
     * @return bool
     */
    protected function check(PostDto $item): bool
    {
        return parent::check($item) && null !== $item->getFromId();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return StatisticsTypeEnum::AVERAGE_NUMBER_POSTS_PER_USER;
    }
}
