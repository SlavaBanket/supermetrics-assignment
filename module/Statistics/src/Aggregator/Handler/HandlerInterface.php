<?php

namespace Statistics\Aggregator\Handler;

use Statistics\Dto\PostDto;
use Statistics\Dto\StatisticsDto;

interface HandlerInterface
{
    /**
     * @param PostDto $item
     *
     * @return void
     */
    public function handle(PostDto $item): void;

    /**
     * @return StatisticsDto
     */
    public function aggregate(): StatisticsDto;

    /**
     * @return string
     */
    public function getType(): string;
}
