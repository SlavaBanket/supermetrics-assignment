<?php

namespace Statistics\Aggregator\Handler;

use Statistics\Dto\PostDto;
use Statistics\Enum\StatisticsTypeEnum;

class LongestPostHandler extends AbstractHandler
{
    /**
     * @var array
     */
    private array $statistics = [];

    /**
     * @param PostDto $item
     *
     * @return void
     */
    public function doHandle(PostDto $item): void
    {
        $month  = $item->getCreatedTime()->format('m');
        $length = strlen($item->getMessage());
        if (empty($this->statistics[$month]) || $length > $this->statistics[$month]['message']) {
            $this->statistics[$month] = ['message' => $item->getMessage(), 'length' => $length];
        }
    }

    /**
     * @return mixed
     */
    protected function doAggregate()
    {
        return $this->statistics;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return StatisticsTypeEnum::LONGEST_POST;
    }
}
