<?php

namespace Statistics\Aggregator\Handler;

use Statistics\Dto\PostDto;
use Statistics\Enum\StatisticsTypeEnum;

class TotalPostsByWeekHandler extends AbstractHandler
{
    /**
     * @var array
     */
    private array $statistics = [];

    /**
     * TODO: The week is displayed if it was in the data. Do I need to display missed weeks???
     *
     * @param PostDto $item
     *
     * @return void
     */
    public function doHandle(PostDto $item): void
    {
        $week = $item->getCreatedTime()->format('W');
        if (!isset($this->statistics[$week])) {
            $this->statistics[$week] = 0;
        }
        $this->statistics[$week]++;
    }

    /**
     * @return mixed
     */
    protected function doAggregate()
    {
        return $this->statistics;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return StatisticsTypeEnum::TOTAL_POSTS_BY_WEEK;
    }
}
