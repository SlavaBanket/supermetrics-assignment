<?php

namespace Statistics\Aggregator;

use Statistics\Aggregator\Handler\HandlerInterface;
use Statistics\Dto\PostDto;

class StatisticsAggregator
{
    /**
     * @var HandlerInterface[]
     */
    private array $handlers = [];

    /**
     * @param PostDto $item
     * @param array   $types
     *
     * @return void
     */
    public function handle(PostDto $item, array $types): void
    {
        foreach ($this->handlers as $handler) {
            //TODO: Implement dynamic addition of handlers to the aggregator by type
            if (in_array($handler->getType(), $types)) {
                $handler->handle($item);
            }
        }
    }

    /**
     * @param array $types
     *
     * @return array
     */
    public function aggregate(array $types): array
    {
        $result = [];
        foreach ($this->handlers as $handler) {
            //TODO: Implement dynamic addition of handlers to the aggregator by type
            if (in_array($handler->getType(), $types)) {
                $result[] = $handler->aggregate();
            }
        }

        return $result;
    }

    /**
     * @param HandlerInterface $handler
     *
     * @return $this
     */
    public function addHandler(HandlerInterface $handler): self
    {
        $this->handlers[] = $handler;

        return $this;
    }
}
