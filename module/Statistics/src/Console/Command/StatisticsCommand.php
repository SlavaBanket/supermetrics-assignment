<?php

namespace Statistics\Console\Command;

use App\Console\Command\Command;
use App\Console\Input\ConsoleInput;
use Statistics\Console\Input\StatisticsInputValidator;
use Statistics\Console\Output\StatisticsRender;
use Statistics\Enum\StatisticsTypeEnum;
use Statistics\Service\StatisticsService;

class StatisticsCommand extends Command
{
    private const LIMIT = 10;

    /**
     * @var string
     */
    protected string $signature = 'statistics limit= types=';

    /**
     * @var string
     */
    protected string $description = 'Calculate statistics from API.' . PHP_EOL .
    'Available types are average_length, average_number_posts_per_user, longest_post, total_posts_by_week' . PHP_EOL .
    'For example: statistics limit=1 types=average_length,average_number_posts_per_user,longest_post,total_posts_by_week';

    /**
     * @var StatisticsService
     */
    private StatisticsService $service;

    /**
     * @var StatisticsRender
     */
    private StatisticsRender $render;

    /**
     * @var StatisticsInputValidator
     */
    private StatisticsInputValidator $validator;

    /**
     * @param StatisticsService        $service
     * @param StatisticsRender         $render
     * @param StatisticsInputValidator $validator
     */
    public function __construct(
        StatisticsService $service,
        StatisticsRender $render,
        StatisticsInputValidator $validator
    ) {
        $this->service   = $service;
        $this->render    = $render;
        $this->validator = $validator;
    }

    /**
     * @param ConsoleInput $input
     *
     * @return int
     */
    public function handle(ConsoleInput $input): int
    {
        if (!$this->validate($input)) {
            return 1;
        }

        $limit  = $input->get('limit', self::LIMIT);
        $result = $this->service->getStatistics($limit, $this->getTypes($input));
        $this->render->render($result);

        return 0;
    }

    /**
     * @param ConsoleInput $input
     *
     * @return bool
     */
    private function validate(ConsoleInput $input): bool
    {
        if ($this->validator->validate($input)) {
            return true;
        }

        foreach ($this->validator->getErrors() as $error) {
            echo $error . PHP_EOL;
        }

        return false;
    }

    /**
     * @param ConsoleInput $input
     *
     * @return array
     */
    private function getTypes(ConsoleInput $input): array
    {
        $types = $input->get('types', StatisticsTypeEnum::all());
        if (!is_array($types)) {
            $types = explode(',', $types);
        }

        return $types;
    }
}
