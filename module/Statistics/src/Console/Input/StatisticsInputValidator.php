<?php

namespace Statistics\Console\Input;

use App\Console\Input\ConsoleInput;
use Statistics\Enum\StatisticsTypeEnum;

class StatisticsInputValidator
{
    /**
     * @var array
     */
    private array $errors;

    /**
     * @param ConsoleInput $input
     *
     * @return bool
     */
    public function validate(ConsoleInput $input): bool
    {
        $this->validateLimit($input);
        $this->validateTypes($input);

        return empty($this->errors);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param ConsoleInput $input
     *
     * @return void
     */
    private function validateLimit(ConsoleInput $input): void
    {
        $value = $input->get('limit');
        if (empty($value)) {
            return;
        }

        if (!isInteger($value)) {
            $this->errors[] = 'Limit should be integer';

            return;
        }

        if (0 >= (int)$value) {
            $this->errors[] = 'Limit should be more than 0';
        }
    }

    /**
     * @param ConsoleInput $input
     *
     * @return void
     */
    private function validateTypes(ConsoleInput $input): void
    {
        $value = $input->get('types');
        if (empty($value)) {
            return;
        }

        $types = explode(',', $value);
        $diff  = array_diff($types, StatisticsTypeEnum::all());
        if (!empty($diff)) {
            $this->errors[] = sprintf('Types %s are not supported', implode(',', $diff));
        }
    }
}