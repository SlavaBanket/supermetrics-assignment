<?php

namespace Statistics\Console\Output\Factory;

use App\Container\Container;
use App\Factory\FactoryInterface;
use Statistics\Console\Output\StatisticsRender;
use Statistics\Console\Output\View\AverageLengthView;
use Statistics\Console\Output\View\AverageNumberPostsPerUserView;
use Statistics\Console\Output\View\LongestPostView;
use Statistics\Console\Output\View\TotalPostsByWeekView;
use Statistics\Enum\StatisticsTypeEnum;

class StatisticsRenderFactory implements FactoryInterface
{
    private const MAP = [
        StatisticsTypeEnum::AVERAGE_LENGTH        => AverageLengthView::class,
        StatisticsTypeEnum::AVERAGE_NUMBER_POSTS_PER_USER => AverageNumberPostsPerUserView::class,
        StatisticsTypeEnum::LONGEST_POST                  => LongestPostView::class,
        StatisticsTypeEnum::TOTAL_POSTS_BY_WEEK           => TotalPostsByWeekView::class,
    ];

    /**
     * @return StatisticsRender
     */
    public function factory(): StatisticsRender
    {
        $render = new StatisticsRender();
        foreach (self::MAP as $key => $view) {
            $render->addView($key, Container::get($view));
        }

        return $render;
    }
}
