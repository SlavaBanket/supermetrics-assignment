<?php

namespace Statistics\Console\Output;

use InvalidArgumentException;
use Statistics\Console\Output\View\StatisticsRenderViewInterface;
use Statistics\Dto\StatisticsDto;

class StatisticsRender
{
    /**
     * @var StatisticsRenderViewInterface[]
     */
    public array $views = [];

    /**
     * @param array $result
     *
     * @return void
     */
    public function render(array $result): void
    {
        /** @var StatisticsDto $statistics */
        foreach ($result as $statistics) {
            if (empty($this->views[$statistics->getType()])) {
                throw new InvalidArgumentException(sprintf('View for type %s not supported', $statistics->getType()));
            }

            $this->views[$statistics->getType()]->render($statistics);
        }
    }

    /**
     * @param string                        $key
     * @param StatisticsRenderViewInterface $view
     *
     * @return $this
     */
    public function addView(string $key, StatisticsRenderViewInterface $view): self
    {
        $this->views[$key] = $view;

        return $this;
    }
}
