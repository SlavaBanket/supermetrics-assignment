<?php

namespace Statistics\Console\Output\View;

use App\Console\Output\Enum\ColorEnum;
use App\Console\Output\Helper\ColorWrapper;
use Statistics\Dto\StatisticsDto;

class AverageLengthView implements StatisticsRenderViewInterface
{
    /**
     * @param StatisticsDto $item
     *
     * @return void
     */
    public function render(StatisticsDto $item): void
    {
        echo ColorWrapper::wrap('Average character length of posts per month', ColorEnum::RED) . PHP_EOL . PHP_EOL;
        foreach ($item->getStatistics() as $month => $value) {
            $date = date('F', mktime(0, 0, 0, $month, 10));
            echo sprintf('   %s %f', ColorWrapper::wrap($date . ':', ColorEnum::GREEN), $value) . PHP_EOL;
        }

        echo PHP_EOL . PHP_EOL;
    }
}
