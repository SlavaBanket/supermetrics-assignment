<?php

namespace Statistics\Console\Output\View;

use App\Console\Output\Enum\ColorEnum;
use App\Console\Output\Helper\ColorWrapper;
use Statistics\Dto\StatisticsDto;

class LongestPostView implements StatisticsRenderViewInterface
{
    /**
     * @param StatisticsDto $item
     *
     * @return void
     */
    public function render(StatisticsDto $item): void
    {
        echo ColorWrapper::wrap('Longest post by character length per month', ColorEnum::RED) . PHP_EOL . PHP_EOL;
        foreach ($item->getStatistics() as $month => $value) {
            $date = date('F', mktime(0, 0, 0, $month, 10));
            echo sprintf('   %s', ColorWrapper::wrap($date . ':', ColorEnum::GREEN)) . PHP_EOL;
            echo sprintf('      %s %d', ColorWrapper::wrap('Length:', ColorEnum::BLUE), $value['length']) . PHP_EOL;
            echo sprintf('      %s %s', ColorWrapper::wrap('Message:', ColorEnum::BLUE), $value['message']) . PHP_EOL;
            echo PHP_EOL;
        }
        echo PHP_EOL . PHP_EOL;
    }
}
