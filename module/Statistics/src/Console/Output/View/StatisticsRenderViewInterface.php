<?php

namespace Statistics\Console\Output\View;

use Statistics\Dto\StatisticsDto;

interface StatisticsRenderViewInterface
{
    /**
     * @param StatisticsDto $item
     *
     * @return void
     */
    public function render(StatisticsDto $item): void;
}
