<?php

namespace Statistics\Console\Output\View;

use App\Console\Output\Enum\ColorEnum;
use App\Console\Output\Helper\ColorWrapper;
use Statistics\Dto\StatisticsDto;

class TotalPostsByWeekView implements StatisticsRenderViewInterface
{
    /**
     * @param StatisticsDto $item
     *
     * @return void
     */
    public function render(StatisticsDto $item): void
    {
        echo ColorWrapper::wrap('Total posts split by week number', ColorEnum::RED) . PHP_EOL . PHP_EOL;
        foreach ($item->getStatistics() as $week => $value) {
            echo sprintf('   %s %d', ColorWrapper::wrap($week . ':', ColorEnum::GREEN), $value) . PHP_EOL;
        }
        echo PHP_EOL . PHP_EOL;
    }
}
