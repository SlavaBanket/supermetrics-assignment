<?php

namespace Statistics\Dto;

use DateTime;

class PostDto
{
    /**
     * @var string|null
     */
    private ?string $id;

    /**
     * @var string|null
     */
    private ?string $fromId;

    /**
     * @var string|null
     */
    private ?string $fromName;

    /**
     * @var string|null
     */
    private ?string $message;

    /**
     * @var string|null
     */
    private ?string $type;

    /**
     * @var DateTime|null
     */
    private ?DateTime $createdTime;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     *
     * @return $this
     */
    public function setId(?string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFromId(): ?string
    {
        return $this->fromId;
    }

    /**
     * @param string|null $fromId
     *
     * @return $this
     */
    public function setFromId(?string $fromId): self
    {
        $this->fromId = $fromId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFromName(): ?string
    {
        return $this->fromName;
    }

    /**
     * @param string|null $fromName
     *
     * @return $this
     */
    public function setFromName(?string $fromName): self
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     *
     * @return $this
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return $this
     */
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedTime(): ?DateTime
    {
        return $this->createdTime;
    }

    /**
     * @param DateTime|null $createdTime
     *
     * @return $this
     */
    public function setCreatedTime(?DateTime $createdTime): self
    {
        $this->createdTime = $createdTime;

        return $this;
    }
}
