<?php

namespace Statistics\Dto;

class StatisticsDto
{
    /**
     * @var string
     */
    private string $type;

    /**
     * @var mixed
     */
    private $statistics;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatistics()
    {
        return $this->statistics;
    }

    /**
     * @param mixed $statistics
     *
     * @return $this
     */
    public function setStatistics($statistics): self
    {
        $this->statistics = $statistics;

        return $this;
    }
}
