<?php

namespace Statistics\Enum;

class StatisticsTypeEnum
{
    public const AVERAGE_LENGTH = 'average_length';

    public const AVERAGE_NUMBER_POSTS_PER_USER = 'average_number_posts_per_user';

    public const LONGEST_POST = 'longest_post';

    public const TOTAL_POSTS_BY_WEEK = 'total_posts_by_week';

    public static function all()
    {
        return [
            self::AVERAGE_LENGTH,
            self::AVERAGE_NUMBER_POSTS_PER_USER,
            self::LONGEST_POST,
            self::TOTAL_POSTS_BY_WEEK,
        ];
    }
}
