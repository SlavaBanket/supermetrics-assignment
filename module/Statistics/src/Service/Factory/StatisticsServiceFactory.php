<?php

namespace Statistics\Service\Factory;

use App\Container\Container;
use App\Factory\FactoryInterface;
use Statistics\Aggregator\StatisticsAggregator;
use Statistics\Service\StatisticsService;
use Supermetrics\Adapter\SupermetricsAdapter;

class StatisticsServiceFactory implements FactoryInterface
{
    /**
     * @return StatisticsService
     */
    public function factory(): StatisticsService
    {
        /** @var SupermetricsAdapter $adapter */
        $adapter = Container::get(SupermetricsAdapter::class);
        /** @var StatisticsAggregator $aggregator */
        $aggregator = Container::get(StatisticsAggregator::class);

        return new StatisticsService($adapter, $aggregator);
    }
}
