<?php

namespace Statistics\Service;

use Statistics\Adapter\PostAdapterInterface;
use Statistics\Aggregator\StatisticsAggregator;

class StatisticsService
{
    /**
     * @var PostAdapterInterface
     */
    private PostAdapterInterface $adapter;

    /**
     * @var StatisticsAggregator
     */
    private StatisticsAggregator $aggregator;

    /**
     * @param PostAdapterInterface $adapter
     * @param StatisticsAggregator $aggregator
     */
    public function __construct(PostAdapterInterface $adapter, StatisticsAggregator $aggregator)
    {
        $this->adapter    = $adapter;
        $this->aggregator = $aggregator;
    }

    /**
     * @param int   $limit
     * @param array $types
     *
     * @return array
     */
    public function getStatistics(int $limit, array $types): array
    {
        $page = 1;
        while ($limit >= $page) {
            $posts = $this->adapter->getPosts($page);
            if (!$posts->valid()) {
                break;
            }

            foreach ($posts as $post) {
                $this->aggregator->handle($post, $types);
            }
            $page++;
        }

        return $this->aggregator->aggregate($types);
    }
}
