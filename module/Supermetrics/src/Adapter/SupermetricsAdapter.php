<?php

namespace Supermetrics\Adapter;

use Generator;
use Statistics\Adapter\PostAdapterInterface;
use Supermetrics\Assembler\PostAssembler;
use Supermetrics\Driver\SupermetricsDriver;

class SupermetricsAdapter implements PostAdapterInterface
{
    /**
     * @var SupermetricsDriver
     */
    private SupermetricsDriver $driver;

    /**
     * @var PostAssembler
     */
    private PostAssembler $assembler;

    /**
     * @param SupermetricsDriver $driver
     * @param PostAssembler      $assembler
     */
    public function __construct(SupermetricsDriver $driver, PostAssembler $assembler)
    {
        $this->driver    = $driver;
        $this->assembler = $assembler;
    }

    /**
     * @param int $page
     *
     * @return Generator|null
     */
    public function getPosts(int $page): ?Generator
    {
        $posts = $this->driver->getPosts($page);
        if (null === $posts) {
            return null;
        }

        yield from $this->assembler->assembleList($posts);
    }
}
