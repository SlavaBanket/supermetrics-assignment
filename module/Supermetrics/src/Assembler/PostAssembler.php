<?php

namespace Supermetrics\Assembler;

use DateTime;
use Generator;
use Statistics\Dto\PostDto;

class PostAssembler
{
    /**
     * @param array $item
     *
     * @return PostDto
     */
    public function assemble(array $item): PostDto
    {
        $dto = new PostDto();
        $dto->setId($this->getValue($item, 'id'))
            ->setType($this->getValue($item, 'type'))
            ->setFromId($this->getValue($item, 'from_id'))
            ->setFromName($this->getValue($item, 'from_name'))
            ->setCreatedTime($this->assembleDate($item))
            ->setMessage($this->getValue($item, 'message'));

        return $dto;
    }

    /**
     * @param array $list
     *
     * @return Generator|PostDto[]
     */
    public function assembleList(array $list): Generator
    {
        foreach ($list as $item) {
            yield $this->assemble($item);
        }
    }

    /**
     * @param array $item
     *
     * @return DateTime|null
     */
    private function assembleDate(array $item): ?DateTime
    {
        $date = $this->getValue($item, 'created_time');
        $date = DateTime::createFromFormat(DATE_ATOM, $date);

        return false === $date ? null : $date;
    }

    /**
     * @param array  $item
     * @param string $key
     *
     * @return mixed|null
     */
    private function getValue(array $item, string $key)
    {
        return $item[$key] ?? null;
    }
}
