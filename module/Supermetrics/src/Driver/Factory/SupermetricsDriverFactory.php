<?php

namespace Supermetrics\Driver\Factory;

use App\Cache\Decorator\CacheDecorator;
use App\Client\HttpClient;
use App\Config\Config;
use App\Container\Container;
use App\Factory\FactoryInterface;
use InvalidArgumentException;
use Supermetrics\Driver\SupermetricsDriver;

class SupermetricsDriverFactory implements FactoryInterface
{
    /**
     * @return SupermetricsDriver
     */
    public function factory(): SupermetricsDriver
    {
        $host = Config::get('client.supermetrics.host');
        if (null === $host) {
            throw new InvalidArgumentException('Supermetrics api host is not configure');
        }
        /** @var HttpClient $client */
        $client = Container::get(HttpClient::class);
        $client->setHost($host);

        /** @var CacheDecorator $cache */
        $cache = Container::get(CacheDecorator::class);

        return new SupermetricsDriver($client, $cache);
    }
}
