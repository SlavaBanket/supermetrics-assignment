<?php

namespace Supermetrics\Driver;

use App\Cache\Decorator\CacheDecorator;
use App\Client\Dto\Request;
use App\Client\Enum\HttpMethodEnum;
use App\Client\Exception\HttpClientException;
use App\Client\HttpClient;
use App\Config\Config;

use RuntimeException;
use Supermetrics\Exception\InvalidTokenException;

class SupermetricsDriver
{
    private const REGISTER_TOKEN_ENDPOINT = '/assignment/register';

    private const POSTS_ENDPOINT = '/assignment/posts';

    private const TOKEN_CACHE_PREFIX = 'token';

    private const POST_CACHE_PREFIX = 'posts';

    /**
     * @var HttpClient
     */
    private HttpClient $client;

    /**
     * @var CacheDecorator
     */
    private CacheDecorator $cache;

    /**
     * @param HttpClient     $client
     * @param CacheDecorator $cache
     */
    public function __construct(HttpClient $client, CacheDecorator $cache)
    {
        $this->client = $client;
        $this->cache  = $cache;
    }

    /**
     * @param int $page
     *
     * @return array|null
     */
    public function getPosts(int $page): ?array
    {
        try {
            return $this->getPostsData($page);
        } catch (InvalidTokenException $ex) {
            $this->refreshToken();

            return $this->getPostsData($page);
        }
    }

    /**
     * @param int $page
     *
     * @return array|null
     */
    private function getPostsData(int $page): ?array
    {
        return $this->cache->getData(
            function () use ($page): ?array {
                return $this->getPostsFromApi($page);
            },
            $this->cache->assembleCacheKey([self::POST_CACHE_PREFIX, $page]),
            Config::get('cache.supermetrics.posts.lifetime')
        );
    }

    /**
     * @param int $page
     *
     * @throws HttpClientException
     * @throws InvalidTokenException
     *
     * @return array|null
     */
    private function getPostsFromApi(int $page): ?array
    {
        $token   = $this->getToken();
        $request = new Request(HttpMethodEnum::GET, self::POSTS_ENDPOINT);
        $request->setQuery(['sl_token' => $token, 'page' => $page]);

        try {
            $response = $this->client->send($request);
        } catch (HttpClientException $ex) {
            if ($this->isInvalidToken($ex->getResponse())) {
                throw new InvalidTokenException();
            }

            throw $ex;
        }

        if ($page !== $response['data']['page']) {
            return null;
        }

        return $response['data']['posts'] ?? null;
    }

    /**
     * @return string
     */
    private function getToken(): string
    {
        return $this->cache->getData(
            function (): string {
                return $this->registerToken();
            },
            $this->cache->assembleCacheKey([self::TOKEN_CACHE_PREFIX]),
            Config::get('cache.supermetrics.token.lifetime')
        );
    }

    /**
     * @return string
     */
    private function refreshToken(): string
    {
        $this->cache->delete($this->cache->assembleCacheKey([self::TOKEN_CACHE_PREFIX]));

        return $this->getToken();
    }

    /**
     * @throws HttpClientException
     *
     * @return string
     */
    private function registerToken(): string
    {
        $auth    = Config::get('client.supermetrics.auth');
        $request = new Request(HttpMethodEnum::POST, self::REGISTER_TOKEN_ENDPOINT);
        $request->setBody($auth);

        $response = $this->client->send($request);

        if (empty($response['data']['sl_token'])) {
            throw new RuntimeException('An error occurred while getting token');
        }

        return $response['data']['sl_token'];
    }

    /**
     * @param string|null $response
     *
     * @return bool
     */
    private function isInvalidToken(?string $response = null): bool
    {
        if (null === $response) {
            return false;
        }

        $result = json_decode($response, true);
        if (false === $result) {
            return false;
        }

        return isset($result['error']['message']) && 'Invalid SL Token' === $result['error']['message'];
    }
}
