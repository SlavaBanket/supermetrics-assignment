<?php

namespace Supermetrics\Exception;

use Exception;

class InvalidTokenException extends Exception
{
}
