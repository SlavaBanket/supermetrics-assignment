## Local setup

This project uses [Docker](https://docs.docker.com/install/) 
and [Docker Compose](https://docs.docker.com/compose/install/). 
Ensure you have both installed.

#### Setup steps

##### 1. .env file deployment
Copy `.env.dist` file into `.env` file. Update parameters if needed. 
This will prepare env variables required for installation.

##### 2. Environment setup
Run `docker-compose up -d` within project's root directory.

This will deploy all necessary containers to run application

##### 3. Application setup
Run `.docker/composer.sh install` to build all the project's dependencies. 
This will install composer dependencies.

##### 4. Application shutdown
To shutdown application and deployed environment run `docker-compose down`
within project's root directory

## Run application

- Run `.docker/console.sh bin/console.php` to show all commands.
- Run `.docker/console.sh bin/console.php COMMAND` to execute command. 
For example `.docker/console.sh bin/console.php statistics limit=1 types=average_length,average_number_posts_per_user,longest_post,total_posts_by_week`.

## Testing

- Run `.docker/codecept.sh run` to test.
