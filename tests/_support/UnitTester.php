<?php

use Codeception\Actor;
use Codeception\Lib\Friend;
use Codeception\Test\Unit;
use Statistics\Dto\PostDto;
use Supermetrics\Assembler\PostAssembler;

/**
 * Inherited Methods
 *
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class UnitTester extends Actor
{
    use _generated\UnitTesterActions;

    private array $post = [
        'id'           => 'id',
        'from_name'    => 'from_name',
        'from_id'      => 'from_id',
        'message'      => 'message',
        'type'         => 'type',
        'created_time' => '2020-09-17T19:22:32+03:00',
    ];

    /**
     * @return array
     */
    public function getPost(): array
    {
        return $this->post;
    }

    /**
     * @return PostDto
     */
    public function getPostDto(): PostDto
    {
        return (new PostAssembler())->assemble($this->post);
    }

    /**
     * @param PostDto $postDto
     * @param array   $post
     * @param Unit    $assert
     *
     * @return void
     */
    public function comparePostDtoWithRawPost(PostDto $postDto, array $post, Unit $assert): void
    {
        $assert->assertTrue($postDto->getId() === $post['id'], 'Id is wrong');
        $assert->assertTrue($postDto->getFromName() === $post['from_name'], 'from_name is wrong');
        $assert->assertTrue($postDto->getFromId() === $post['from_id'], 'from_id is wrong');
        $assert->assertTrue($postDto->getMessage() === $post['message'], 'message is wrong');
        $assert->assertTrue($postDto->getType() === $post['type'], 'type is v');
        $assert->assertTrue(
            $postDto->getCreatedTime()->format(DateTime::ATOM) === $post['created_time'],
            'created_time is wrong'
        );
    }
}
