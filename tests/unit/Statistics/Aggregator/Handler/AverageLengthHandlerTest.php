<?php

namespace Tests\Unit\Statistics\Aggregator\Handler;

use Statistics\Aggregator\Handler\AverageLengthHandler;
use Codeception\Test\Unit;
use Statistics\Enum\StatisticsTypeEnum;
use UnitTester;

class AverageLengthHandlerTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @return void
     */
    public function testAggregate(): void
    {
        $handler = new AverageLengthHandler();
        $post    = $this->tester->getPostDto();
        $handler->handle($post);

        $result = $handler->aggregate();

        $this->assertEquals(StatisticsTypeEnum::AVERAGE_LENGTH, $result->getType());

        $month = $post->getCreatedTime()->format('m');
        $this->assertEquals(strlen($post->getMessage()), $result->getStatistics()[$month]);
    }
}
