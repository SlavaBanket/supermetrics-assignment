<?php

namespace Tests\Unit\Statistics\Aggregator\Handler;

use Codeception\Test\Unit;
use Statistics\Aggregator\Handler\AverageNumberPostsPerUserHandler;
use Statistics\Enum\StatisticsTypeEnum;
use UnitTester;

class AverageNumberPostsPerUserHandlerTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @return void
     */
    public function testAggregate(): void
    {
        $handler = new AverageNumberPostsPerUserHandler();
        $post    = $this->tester->getPostDto();
        $handler->handle($post);

        $result = $handler->aggregate();

        $this->assertEquals(StatisticsTypeEnum::AVERAGE_NUMBER_POSTS_PER_USER, $result->getType());

        $month = $post->getCreatedTime()->format('m');
        $this->assertEquals(1, $result->getStatistics()[$month]);
    }
}
