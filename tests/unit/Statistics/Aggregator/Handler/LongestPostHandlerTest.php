<?php

namespace Tests\Unit\Statistics\Aggregator\Handler;

use Codeception\Test\Unit;
use Statistics\Aggregator\Handler\LongestPostHandler;
use Statistics\Enum\StatisticsTypeEnum;
use UnitTester;

class LongestPostHandlerTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @return void
     */
    public function testAggregate(): void
    {
        $handler = new LongestPostHandler();
        $post    = $this->tester->getPostDto();
        $handler->handle($post);

        $result = $handler->aggregate();

        $this->assertEquals(StatisticsTypeEnum::LONGEST_POST, $result->getType());

        $month = $post->getCreatedTime()->format('m');
        $this->assertEquals(strlen($post->getMessage()), $result->getStatistics()[$month]['length']);
        $this->assertEquals($post->getMessage(), $result->getStatistics()[$month]['message']);
    }
}
