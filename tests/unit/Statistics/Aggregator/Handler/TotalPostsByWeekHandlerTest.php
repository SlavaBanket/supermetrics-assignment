<?php

namespace Tests\Unit\Statistics\Aggregator\Handler;

use Codeception\Test\Unit;
use Statistics\Aggregator\Handler\TotalPostsByWeekHandler;
use Statistics\Enum\StatisticsTypeEnum;
use UnitTester;

class TotalPostsByWeekHandlerTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @return void
     */
    public function testAggregate(): void
    {
        $handler = new TotalPostsByWeekHandler();
        $post    = $this->tester->getPostDto();
        $handler->handle($post);

        $result = $handler->aggregate();

        $this->assertEquals(StatisticsTypeEnum::TOTAL_POSTS_BY_WEEK, $result->getType());

        $week = $post->getCreatedTime()->format('W');
        $this->assertEquals(1, $result->getStatistics()[$week]);
    }
}
