<?php

namespace Tests\Unit\Statistics\Service;

use Codeception\Test\Unit;
use Mockery;
use Statistics\Aggregator\Handler\AverageLengthHandler;
use Statistics\Aggregator\Handler\AverageNumberPostsPerUserHandler;
use Statistics\Aggregator\Handler\LongestPostHandler;
use Statistics\Aggregator\Handler\TotalPostsByWeekHandler;
use Statistics\Aggregator\StatisticsAggregator;
use Statistics\Enum\StatisticsTypeEnum;
use Statistics\Service\StatisticsService;
use Supermetrics\Adapter\SupermetricsAdapter;
use Supermetrics\Assembler\PostAssembler;
use Supermetrics\Driver\SupermetricsDriver;
use UnitTester;

class StatisticsServiceTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @return void
     */
    public function testGetStatistics(): void
    {
        $post = $this->tester->getPostDto();

        $driver = Mockery::mock(SupermetricsDriver::class);
        $driver->shouldReceive('getPosts')->andReturn([$this->tester->getPost()]);

        $assembler = new PostAssembler();
        $adapter   = new SupermetricsAdapter($driver, $assembler);

        $aggregator = new StatisticsAggregator();
        $this->addHandlers($aggregator);

        $service = new StatisticsService($adapter, $aggregator);
        $result  = $service->getStatistics(1, StatisticsTypeEnum::all());

        $this->assertEquals(4, count($result));

        $month = $post->getCreatedTime()->format('m');

        $this->assertEquals(StatisticsTypeEnum::AVERAGE_LENGTH, $result[0]->getType());
        $this->assertEquals(strlen($post->getMessage()), $result[0]->getStatistics()[$month]);

        $this->assertEquals(StatisticsTypeEnum::AVERAGE_NUMBER_POSTS_PER_USER, $result[1]->getType());
        $this->assertEquals(1, $result[1]->getStatistics()[$month]);

        $this->assertEquals(StatisticsTypeEnum::LONGEST_POST, $result[2]->getType());
        $this->assertEquals(strlen($post->getMessage()), $result[2]->getStatistics()[$month]['length']);
        $this->assertEquals($post->getMessage(), $result[2]->getStatistics()[$month]['message']);

        $this->assertEquals(StatisticsTypeEnum::TOTAL_POSTS_BY_WEEK, $result[3]->getType());
        $week = $post->getCreatedTime()->format('W');
        $this->assertEquals(1, $result[3]->getStatistics()[$week]);
    }

    /**
     * @param StatisticsAggregator $aggregator
     *
     * @return void
     */
    private function addHandlers(StatisticsAggregator $aggregator): void
    {
        $aggregator->addHandler(new AverageLengthHandler());
        $aggregator->addHandler(new AverageNumberPostsPerUserHandler());
        $aggregator->addHandler(new LongestPostHandler());
        $aggregator->addHandler(new TotalPostsByWeekHandler());
    }
}
