<?php

namespace Tests\Unit\Supermetrics\Adapter;

use Codeception\Test\Unit;
use Mockery;
use Supermetrics\Adapter\SupermetricsAdapter;
use Supermetrics\Assembler\PostAssembler;
use Supermetrics\Driver\SupermetricsDriver;
use UnitTester;

class SupermetricsAdapterTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @return void
     */
    public function testGetPosts(): void
    {
        $driver = Mockery::mock(SupermetricsDriver::class);
        $driver->shouldReceive('getPosts')->andReturn([$this->tester->getPost()]);

        $assembler = new PostAssembler();
        $adapter   = new SupermetricsAdapter($driver, $assembler);
        $result    = $adapter->getPosts(1);
        foreach ($result as $item) {
            $this->tester->comparePostDtoWithRawPost($item, $this->tester->getPost(), $this);
        }
    }
}
