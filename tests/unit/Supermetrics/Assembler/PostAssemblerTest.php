<?php

namespace Tests\Unit\Supermetrics\Assembler;

use Codeception\Test\Unit;
use Supermetrics\Assembler\PostAssembler;
use UnitTester;

class PostAssemblerTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @return void
     */
    public function testAssemble(): void
    {
        $assembler = new PostAssembler();
        $item      = $assembler->assemble($this->tester->getPost());
        $this->tester->comparePostDtoWithRawPost($item, $this->tester->getPost(), $this);
    }

    /**
     * @return void
     */
    public function testAssembleList(): void
    {
        $assembler = new PostAssembler();
        $result    = $assembler->assembleList([$this->tester->getPost()]);
        foreach ($result as $item) {
            $this->tester->comparePostDtoWithRawPost($item, $this->tester->getPost(), $this);
        }
    }
}
