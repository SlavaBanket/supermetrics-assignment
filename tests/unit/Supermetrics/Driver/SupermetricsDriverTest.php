<?php

namespace Tests\Unit\Supermetrics\Driver;

use App\Cache\Decorator\CacheDecorator;
use App\Cache\Driver\RedisDriver;
use App\Client\Dto\Request;
use App\Client\HttpClient;
use App\Config\Config;
use Codeception\Test\Unit;
use Mockery;
use Supermetrics\Driver\SupermetricsDriver;

class SupermetricsDriverTest extends Unit
{
    private const REGISTER_TOKEN_ENDPOINT = '/assignment/register';

    private const POSTS_ENDPOINT = '/assignment/posts';

    private const FIELDS = ['id', 'from_name', 'from_id', 'message', 'type', 'created_time'];

    protected function _before()
    {
        require 'helpers/helper.php';
        Config::load();
    }

    /**
     * @return void
     */
    public function testGetPosts(): void
    {
        $client = Mockery::mock(HttpClient::class);
        $client->shouldReceive('send')
            ->andReturnUsing(
                function (Request $request) {
                    return $this->mockSend($request);
                }
            );

        $cacheDriver = Mockery::mock(RedisDriver::class);
        $cacheDriver->shouldReceive('get')->andReturn(false);
        $cacheDriver->shouldReceive('set')->andReturn(true);

        $cache = new CacheDecorator($cacheDriver);

        $driver = new SupermetricsDriver($client, $cache);
        $posts  = $driver->getPosts(1);
        foreach ($posts as $post) {
            $diff = array_diff(self::FIELDS, array_keys($post));
            $this->assertTrue(empty($diff), sprintf('Fields %s not returned', implode(',', $diff)));
        }
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function mockSend(Request $request): array
    {
        $tokenResponse = file_get_contents('tests/_support/Data/token.json');
        $postsResponse = file_get_contents('tests/_support/Data/posts.json');

        switch ($request->getUrl()) {
            case self::REGISTER_TOKEN_ENDPOINT:
                return json_decode($tokenResponse, true);
            case self::POSTS_ENDPOINT:
                return json_decode($postsResponse, true);
            default:
                $this->assertFalse(true, sprintf('Data for url %s not created', $request->getUrl()));
        }
    }
}
